// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  onAuthStateChanged,
} from "firebase/auth";

import axios from "axios";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAwEGcBSh6bvmecAjXkNiGoRRehnznDFD0",
  authDomain: "final409-17fe2.firebaseapp.com",
  projectId: "final409-17fe2",
  storageBucket: "final409-17fe2.appspot.com",
  messagingSenderId: "205374837174",
  appId: "1:205374837174:web:e4f450c41bd5940a4ab117",
  measurementId: "G-Y2MMQGZ9F5",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

// ----------------------------------------

// Initialize Axios
const BaseApi = axios.create({
  baseURL: `https://finalproject-backend.vercel.app/api/`,
});

// For Processing Signup Request
const signUp = (email, password) => {
  const auth = getAuth();
  createUserWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      // Signed in
      const user = userCredential.user;
      alert("user created");
      // ...

      // Post user data to MongoDB
      BaseApi.post(`/users`, {
        firebaseId: user.uid,
        email: email,
      })
        .then(function (response) {
          console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        });
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      alert(errorMessage);
      // ..
    });
};

// export default signUp;

const login = (email, password) => {
  const auth = getAuth();
  signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      // Signed in
      const user = userCredential.user;
      alert("User logged in");
      // ...
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      alert(errorMessage);
    });
};

// Checking if User is logged in

const isLoggedIn = () => {
  const auth = getAuth();
  const user = auth.currentUser;

  //   console.log(user);

  return user;
};

export { signUp, login, isLoggedIn };
