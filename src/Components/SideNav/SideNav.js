import { getAuth } from 'firebase/auth';
import React from 'react';
import {
    FaUserAlt,
    FaUpload,
    FaCommentAlt,
    FaBookOpen
} from "react-icons/fa";
import { NavLink } from "react-router-dom";
import llamaBearIcon from '../../Pages/LandingPage/Llama_Bear_Light_Mode.png';
import './SideNav.css';

const SideNav = ({children}) => {
    const auth = getAuth();
    const user = auth.currentUser;
    const currentUserId = user.uid;

    const menuItem = [
        {
            path:`/AboutMe/${currentUserId}`,
            name:"Profile",
            icon:<FaUserAlt/> 
        },
        {
            // somewhat of a home page
            path:"/Upload",
            name:"Upload",
            icon:<FaUpload/>
        },
        {
            path:"/Review",
            name:"Review",
            icon:<FaCommentAlt/> 
        },
        {
            path:"/Templates",
            name:"Examples",
            icon:<FaBookOpen/> 
        },
    ];
    return (
        <div className='sidebar-container'>
            <div className='sidebar'>
                <div className='top-section'>

                    <NavLink className='home-link' to="/">
                        <h3 className='logo'>LlamaBear</h3>
                        <img src={llamaBearIcon} className='sidenav-icon'></img>
                    </NavLink>

                </div>
                {
                    menuItem.map((item, index) => (
                        <NavLink to={item.path} key={index} className="sidenav-link" activeclassname="active">
                            <div className='icon'>{item.icon}</div>
                            <div className='link_text'>{item.name}</div>
                        </NavLink>
                    ))
                }
            </div>
            <main>{children}</main>
        </div>
    );
};

export default SideNav;