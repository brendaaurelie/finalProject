import React, { useEffect } from "react";
import "./AboutMe.css";
import { Link, useParams } from "react-router-dom";
import pdf_icon from './pdf-icon.png'
import { Rating, TextField } from "@mui/material";
import { getAuth } from "firebase/auth";
import axios from 'axios';
import { Chip } from "@mui/material";
import Box from "@mui/material/Box";

export default function AboutMe() {
    const [pageOwner, setPageOwner] = React.useState({});
    const [reviews, setReviews] = React.useState([]);
    const [resumes, setResumes] = React.useState([]);

    const { id } = useParams();

    const auth = getAuth();
    const user = auth.currentUser;
    const currentUserId = user.uid;

    let fullPdf = []
    const userIsAuthor = currentUserId === id;

    // console.log('id:', id);
    // console.log('currentUserId:', currentUserId);
    // console.log('userIsAuthor:', userIsAuthor);

    // Fetch API DATA
    useEffect(() => {
        fetchPageOwnerData(id);
        fetchUserReviews(id);
        fetchUserPdfs();
    }, []);
    
    // Funcions
    function fetchPageOwnerData(id) {
        return axios.get(`https://finalproject-backend.vercel.app/api/users/?where=%7B%22firebaseId": "${id}"}`)
        .then((res) => {
            if (res.data) {
                // console.log("FETCH_USER_DATA:", res.data.data[0]);
                setPageOwner(res.data.data[0]);
                return Promise.resolve(res.data);
            }
            return Promise.reject(res);
        })
        .catch((res) => {
            // console.log("FETCH_USER_DATA:", res);
            setPageOwner({});
            return Promise.reject(res);
        });
    }
    
    function fetchUserReviews(id) {
        // console.log('fetchUserReviews');
        return axios.get(`https://finalproject-backend.vercel.app/api/comments/?where=%7B%22firebaseId": "${id}"}`)
        .then((res) => {
            if (res.data) {
                console.log("FETCH_USER_REVIEWS:", res.data.data);
                setReviews(res.data.data);
                return Promise.resolve(res.data.data);
            }
            return Promise.reject(res);
        })
        .catch((res) => {
            // console.log("FETCH_USER_REVIEWS:", res);
            setReviews([]);
            return Promise.reject(res);
        });
    }

    

    let fetchUserPdfs = () => {
       
        axios.get(`https://finalproject-backend.vercel.app/api/resumes/?where={"firebaseId":"${currentUserId}", "anonymity":${false}}`).then((response) => {
            console.log("FETCH_USER_RESUMES:", response);
            response.data.data.forEach(element => {
                let src = `https://finalproject-backend.vercel.app/api/resumes/pdf/${element._id}`;

                const pdf = ({
                    resume_name: element.documentName,
                    pdf_src: src,
                    tags: element.tags.map(tag => {
                        return tag.toLowerCase();
                    }),
                    path: `/Resume/${element._id}`
                });
                fullPdf = [...fullPdf,pdf]
                setResumes(fullPdf);
            });
        })
    }

    const updateAboutMe = (value) => {
        // console.log('updateAboutMe', value);
        // API CALL HERE
        axios.put(`https://finalproject-backend.vercel.app/api/users/${pageOwner._id}`, { aboutme: value });

        // Update locally
        let copy_owner = {...pageOwner}
        copy_owner.aboutme = value;
        setPageOwner(copy_owner);
    };

    const updateName = (value) => {
        // console.log('updateName:', value);
        // API CALL HERE
        axios.put(`https://finalproject-backend.vercel.app/api/users/${pageOwner._id}`, { username: value });

        // Update locally
        let copy_owner = {...pageOwner}
        copy_owner.username = value;
        setPageOwner(copy_owner);
    };

    // console.log('pageOwner RENDER', pageOwner);

    return (
        <div className="about-me-page">
            {
                !userIsAuthor ?
                <h1 className="user-name">{pageOwner.username}</h1>
                :
                <div className="about-me-page">
                    <TextField 
                    id="name_field" 
                    variant="standard"  
                    multiline
                    value={pageOwner.username}
                    onChange={(event) => {
                        updateName(event.target.value);
                    }}
                />
                </div>
            }

            <p className="section-header">About Me</p>
            <div className="section">
                <TextField 
                id="outlined-basic" 
                variant="filled" 
                fullWidth 
                multiline
                value={pageOwner.aboutme}
                inputProps={{
                    readOnly: !userIsAuthor,
                    disabled: !userIsAuthor
                }}
                InputProps={{
                    disabled: !userIsAuthor,
                    inputProps: { style: { color: '#ffff' }}
                }}
                onChange={(event) => {
                    updateAboutMe(event.target.value);
                }}
                />
            </div>

            <p className="section-header">Resume</p>
            <div className="section">
                <div className="upload-container">
                {
                    resumes ? resumes.map((resume, index) => {
                        return (
                        <Link to={resume.path} key={index}>
                            <div className="pdf-card">
                                <embed src={resume.pdf_src} type="application/pdf" className="pdf" />
                                <h3 className="resume-name">{resume.resume_name}</h3>
                                <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                                    {resume.tags.map((value) => (
                                        <Chip
                                            key={value}
                                            label={value}
                                            style={{ backgroundColor: '#FF7171', marginBottom: '1%' }}
                                        />
                                    ))}
                                </Box>
                            </div>
                        </Link>)
                    }) : <></>
                }
                </div>
            </div>
            
            <p className="section-header">Reviews</p>
            <div className="section">
                <ul className='review-list'>
                    {
                        reviews ? reviews.map((review, idx) => {
                            return (
                                <li key={idx.toString()}>
                                    <Link to={`/resume/${review.resumeId}`} style={{ textDecoration: 'none' }}>
                                        <div className="review-item">
                                            <Rating
                                            name="simple-controlled"
                                            value={review.rating}
                                            readOnly
                                            className="review-rating"
                                            />

                                            <p> {'\"' + review.content + '\"'} </p>
                                        </div>
                                    </Link>
                                </li>
                            )
                        }) : <></>
                    }
                </ul>
            </div>
        </div>
    );
}