import React, {useState, useEffect} from "react";
import Axios from "axios";
import "./Templates.css";


export default function Templates() {
    const [templates, setTemplates] = useState([]);

    useEffect(() => {
        populateTemplates();
    }, [])

    let populateTemplates = () => {
        setTemplates([]);
        Axios.get("https://finalproject-backend.vercel.app/api/templates/").then((response) => {
            response.data.data.forEach(element => {
                console.log(element)
                let src = `https://finalproject-backend.vercel.app/api/templates/pdf/${element._id}`;

                const pdf = ({
                    resume_name: element.documentName,
                    pdf_src: src,
                    tags: element.tags.map(tag => {
                        return tag.toLowerCase();
                    }),
                    path: `/templates/${element._id}`
                })
                setTemplates([...templates, pdf])
            });
        })
    }

    return (
        <div className="templates-page">
            <h1 className="title">Templates</h1>

            <div className="template-gallery">
            {
                templates ? templates.map((template) => {
                    return (
                        <div className="pdf-item">
                            <embed src={template.pdf_src} type="application/pdf" className="pdf" />
                        </div>
                    )
                }) : <></>
            }
            </div>
        </div>
    );
};
