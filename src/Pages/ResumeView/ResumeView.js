import { Autocomplete, Button, Rating, TextField } from "@mui/material";
import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import { getAuth } from "firebase/auth";
import "./ResumeView.css";
import axios from "axios";

export default function ResumeView() {
    const [resumeData, setResumeData] = React.useState({tags:[]});
    const [userIsAuthor, setUserIsAuthor] = React.useState(false);

    const [review, setReview] = React.useState({content: '', firebaseId: '', rating: 4, resumeId: ''});
    const [rating, setRating] = React.useState(2);
    const [commentText, setCommentText] = React.useState("");

    const [userReviews, setUserReviews] = React.useState([]);

    const { id } = useParams();

    const auth = getAuth();
    const user = auth.currentUser;
    const currentUserId = user.uid;

    // let userReviews = [];
    // for (let i = 0; i < 5; ++i) {
    //     userReviews.push({rating: i, comment: 'sagdfhsdfv ;laksdnjcvoas;fd jkpaojwfawpdofmaskl; cvamskl;fakfcvl asdfasdf'});
    // }

    // Fetch API DATA
    useEffect(() => {
        fetchResumeData(id);
        fetchUserReviews(id);
        fetchOwnReview();
    }, []);

    // Functions
    function fetchResumeData(id) {
        return axios.get(`https://finalproject-backend.vercel.app/api/resumes/${id}`)
        .then((res) => {
            if (res.data) {
                // console.log("FETCH_RESUME_DATA:", res.data.data);
                let newResumeData = res.data.data;
                setResumeData(newResumeData);

                // Set view
                // console.log('resumeData.firebaseId', newResumeData.firebaseId)
                // console.log('currentUserId', currentUserId)
                // console.log('resumeData.firebaseId === currentUserId', newResumeData.firebaseId === currentUserId);
                setUserIsAuthor(newResumeData.firebaseId === currentUserId);

                return Promise.resolve(res.data.data);
            }
            return Promise.reject(res);
        })
        .catch((res) => {
            // console.log("FETCH_RESUME_DATA:", res);
            setResumeData({});
            return Promise.reject(res);
        });
    }

    function fetchUserReviews(id) {
        return axios.get(`https://finalproject-backend.vercel.app/api/comments/?where={"resumeId": "${id}"}`)
        .then((res) => {
            if (res.data) {
                // console.log("FETCH_REVIEWS:", res.data.data);
                setUserReviews(res.data.data);
                return Promise.resolve(res.data.data);
            }
            return Promise.reject(res);
        })
        .catch((res) => {
            // console.log("FETCH_REVIEWS:", res);
            setUserReviews([]);
            return Promise.reject(res);
        });
    }

    function fetchOwnReview() {
        axios.get(`https://finalproject-backend.vercel.app/api/comments/?where={"resumeId": "${id}", "firebaseId": "${currentUserId}"}`)
        .then((res) => {
            // console.log("own comment response here", res);
            // console.log("own comment here", res.data.data[0].content);
            setRating(res.data.data[0].rating);
            setCommentText(res.data.data[0].content);
        })
    }

    const updateResumeTags = (value) => {
        console.log('updateResumeTags', value);
        // API CALL HERE
        axios.put(`https://finalproject-backend.vercel.app/api/resumes/${id}`, { tags: value });

        // // Update locally
        let copyResumeData = {...resumeData}
        copyResumeData.tags = value;
        setResumeData(copyResumeData);
    };

    const updateCommentBox = (value) => {
        setCommentText(value);
    }

    const postComment = () => {
        // axios.post(`https://finalproject-backend.vercel.app/api/comments/`);
        axios({
            method: 'post',
            url: 'https://finalproject-backend.vercel.app/api/comments/',
            data: {
                firebaseId: currentUserId,
                resumeId: resumeData._id,
                rating: rating,
                content: commentText
            }
        })
    }

    console.log('resumeData.tags', resumeData.tags)
    return (
        <div className="resume-view-page">
            <div className="side-bar">
                <Link to="/Review">
                    <Button className="nav-button" variant="contained" fullWidth style={{ backgroundColor: '#564BD1', color: '#FAF5EF', borderColor: '#FAF5EF'}}>🡠 Back</Button>
                </Link>
                <div className="tag-group">
                    <span className="sub-section-title"> Tags </span>
                    <Autocomplete
                    multiple
                    id="tags-standard"
                    options={resumeTags}
                    // getOptionLabel={(option) => option.title}
                    limitTags={15}
                    // defaultValue={resumeData.tags}
                    value={resumeData.tags}
                    onChange={(event, newValue) => {
                        updateResumeTags(newValue);
                    }}
                    readOnly={!userIsAuthor}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            variant="standard"
                            // label="Tags"
                            placeholder="Search for tags"
                            style={{color:"#FAF5EF"}}
                        />
                    )}
                    style={{ backgroundColor: '#5147c5', color: '#FAF5EF'}}
                    className="tag-search"
                    />
                </div>
            </div>

            <div className="pdf-frame">
                <iframe
                    src={`https://finalproject-backend.vercel.app/api/resumes/pdf/${id}`}
                    frameBorder="0"
                    scrolling="auto"
                    height="100%"
                    width="100%"
                ></iframe>
            </div>
    
            <div id="right-sidebar" className="side-bar">
                {
                    userIsAuthor ? 
                    <>
                        <span className="sub-section-title">Comments</span>
                        <ul className='review-list'>
                            {
                                userReviews ? userReviews.map((review, idx) => {
                                    return (
                                        <li key={idx.toString()}>
                                            <Link to={`/AboutMe/${review.firebaseId}`}>
                                                <div className="review-item">
                                                    <Rating
                                                    name="simple-controlled"
                                                    value={review.rating}
                                                    readOnly
                                                    className="review-rating"
                                                    />

                                                    <p> {'\"' + review.content + '\"'} </p>
                                                </div>
                                            </Link>
                                        </li>
                                    )
                                }) : <></>
                            }
                        </ul>
                    </>
                    : 
                    <>
                        <span className="sub-section-title">Review this resume</span>
                        <Rating
                        name="simple-controlled"
                        value={rating}
                        onChange={(event, newValue) => {
                            setRating(newValue);
                        }}
                        className="rating"
                        />
                        <p>Add review</p>
                        <TextField id="outlined-basic" label="" variant="filled" multiline maxRows={20} className="text-box" defaultValue={commentText} onChange={(event) => {updateCommentBox(event.target.value);}}/>
                        <Button variant="contained" onClick={postComment} style={{ backgroundColor: '#564BD1', color: '#ffff', borderColor: '#ffff', margin: '2% 0' }}>Submit</Button>
                    </>
                }
            </div>
        </div>
    );
}

const resumeTags = ['Engineering', 'Medicine', 'Education', 'Fine and Applied Arts', 'Informatics', 'Social Work', 'Information Sciences', 'Media', 'Business', 'Law', 'Liberal Arts and Sciences', 'Grad School', 'Part-time', 'Fulltime', 'Animal Sciences', 'Anthropology', 'Astrophysics', 'Astronomy', 'Chemical Engineering', 'Civil Engineering', 'Communication', 'Dance', 'Marketing', 'History', 'Languages', 'Mathematics', 'Neuroscience', 'Physics', 'Statistics', 'Economics', 'Theatre', 'Music', 'Entry Level', 'Project Manager', 'Designer']
