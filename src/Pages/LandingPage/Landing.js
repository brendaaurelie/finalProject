import { AppBar, Button, Icon, Toolbar } from "@mui/material";
import React from "react";
import llamaBearIcon from "./Llama_Bear_Light_Mode.png";
import businessConsultingImg from "./business-consulting.jpg";
import shakingHandsImg from "./shaking-hands.jpg";
import studyingImg from "./studying.jpg";
import "./Landing.css";
import { Box, Container } from "@mui/system";
import { Link } from "react-router-dom";

const Landing = () => {
  //   const [authToken, setAuthToken] = useState();

  return (
    <>
      <AppBar
        className="topBar"
        position="static"
        style={{ backgroundColor: "#564BD1" }}
      >
        <Container className="" maxWidth="xl">
          <Toolbar disableGutters>
            <h3>LlamaBear</h3>

            <Icon style={{ textAlign: "center" }}>
              <img style={{ height: "100%" }} src={llamaBearIcon} />
            </Icon>

            <Box sx={{ flexGrow: 1, display: { md: "flex" } }}></Box>
            <Box sx={{ flexGrow: 0, display: { md: "flex" } }}>
              <Link className="button-link" to="/Login">
                <Button
                  className="login"
                  variant="outlined"
                  color="success"
                  style={{
                    color: "#ffff",
                    borderColor: "#ffff",
                    margin: "0px 5px",
                  }}
                >
                  Login
                </Button>
              </Link>

              <Link className="button-link" to="/SignUp">
                <Button
                  className="button2"
                  variant="outlined"
                  style={{
                    color: "#ffff",
                    borderColor: "#ffff",
                    margin: "0px 5px",
                  }}
                >
                  Sign Up
                </Button>
              </Link>
            </Box>
          </Toolbar>
        </Container>
      </AppBar>

      <section className="first-section">
        <div className="grid-item">
          <p>#1 Resume Review Site, LlamaBear News 2022</p>

          <h1 className="section-header">Great Resumes, Simplified</h1>
          <div style={{ width: "100%", fontSize: "1em" }}>
            <p>
              Connect and collaborate with recruiters and experts of various
              fields worldwide to sculpt the perfect resume. Thousands of
              templates and examples to view for free.
            </p>
          </div>
          <Link to="/SignUp">
            <Button
              variant="contained"
              style={{
                backgroundColor: "#564BD1",
                color: "#ffff",
                borderColor: "#ffff",
                margin: "0px 5px",
              }}
            >
              Get Started
            </Button>
          </Link>
        </div>

        <div className="grid-item">
          <div className="vertical-container">
            <img src={businessConsultingImg}></img>
          </div>
          <div className="vertical-container staggered-container">
            <img src={shakingHandsImg}></img>
          </div>
        </div>
      </section>

      <section className="second-section">
        {/* <div><img src={studyingImg} style={{height: '70%'}}></img></div> */}
        <div className="grid-item">
          <img
            src={studyingImg}
            className="cover-img"
            style={{ height: "70%" }}
          ></img>
        </div>
        <div className="grid-item">
          <h1 className="section-header">
            Proof-reading has never been easier
          </h1>
          <ol>
            <li>
              Make your resume from scratch or download one of our many
              templates.
            </li>
            <li>Upload your resume</li>
            <li>Use other user's feedback to improve your resume!</li>
          </ol>
          <Link className="links" to="/SignUp">
            Get Started 🡢
          </Link>
        </div>
      </section>
    </>
  );
};
  
export default Landing;
