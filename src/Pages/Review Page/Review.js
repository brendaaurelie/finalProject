import "./Review.css";
import React, { useEffect } from "react";
import { useState } from "react";
import { Link } from "react-router-dom"
import { Chip } from "@mui/material";
import Box from "@mui/material/Box";
import Axios from "axios";


const Review = () => {
    const [keyword, setKeyword] = useState("");
    const [displayPdfList, setDisplayPdfList] = useState([]);
    const [isFiltered, setIsFiltered] = useState(false);
    const [permaList, setPermaList] = useState([])

    let fullPdfList = []

    useEffect(() => {
        populatePdfList();
    }, [])
    
    let populatePdfList = () => {
        Axios.get(`https://finalproject-backend.vercel.app/api/resumes/`).then((response) => {
            console.log(response);
            response.data.data.forEach(element => {
                let src = `https://finalproject-backend.vercel.app/api/resumes/pdf/${element._id}`;

                const pdf = ({
                    resume_name: element.documentName,
                    pdf_src: src,
                    tags: element.tags.map(tag => {
                        return tag.toLowerCase();
                    }),
                    path: `/Resume/${element._id}`
                })
                // console.log(pdf)
                fullPdfList = [...fullPdfList, pdf];
                setDisplayPdfList(fullPdfList);
                setPermaList(fullPdfList);
            });
        })
    }

    const updateKeyword = (event) => {
        setKeyword(event.target.value.toLowerCase());
    }

    const search = () => {
        console.log("search keyword is", keyword);
        if (keyword.trim() === "") {
            clearSearch();
        } else {
            setIsFiltered(true);
            let filtered = permaList.filter(d =>
                d.resume_name.toLowerCase().includes(keyword) ||
                d.tags.find(element => element.includes(keyword)));

            // console.log("filtered", filtered);
            setDisplayPdfList(filtered);
        }
    }

    const clearSearch = () => {
        setDisplayPdfList(permaList);
        setIsFiltered(false);
        setKeyword("");
        // console.log("cleared search");
    }

    return (
        <div id="reviewContainer">
            <h1 className="header">Review Others Resumes</h1>

            <div id="search-container">
                <div className="search">
                    <div className="searchInputs">
                        <input type="text" value={keyword} onChange={updateKeyword} />
                    </div>
                </div>
                <button onClick={search}>Search</button>
                {isFiltered ? <button onClick={clearSearch}>Clear Search</button> : <></>}
            </div>

            <div className="pdf-card-grid">
                { 
                    displayPdfList.map((item, index) => (
                        <Link className="link" to={item.path} key={index}>
                            <div className="pdf-card">
                                <embed src={item.pdf_src} type="application/pdf" className="pdf" />
                                <h3 className="resume-name">{item.resume_name}</h3>
                                <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                                    {item.tags.map((value) => (
                                        <Chip
                                            key={value}
                                            label={value}
                                            style={{ backgroundColor: '#FF7171', marginBottom: '1%' }}
                                        />
                                    ))}
                                </Box>
                            </div>
                        </Link>
                    ))
                }
            </div>
        </div>
    )
}

export default Review;