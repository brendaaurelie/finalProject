import React from "react";
import Button from "@mui/material/Button";
import { useState } from "react";
import { TextField } from "@mui/material";
import { makeStyles } from "@material-ui/core";
import { Link, useNavigate } from "react-router-dom";
import { Divider } from "@mui/material";
import "./LogIn.css";

import Alert from "@mui/material/Alert";
import Collapse from "@mui/material/Collapse";

import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import { login, isLoggedIn } from "../auth.js";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    padding: theme.spacing(10),
    "& .MuiTextField-root": {
      margin: theme.spacing(2),
      width: "300px",
    },
    "& .MuiButtonBase-root": {
      margin: theme.spacing(2),
      width: "300px",
    },
  },
}));

const LogIn = () => {
  const classes = useStyles();
  const [registerEmail, setRegisterEmail] = useState("");
  const [registerPassword, setRegisterPassword] = useState("");
  const [user, setUser] = useState({});
  const [success, setSuccess] = useState(false);
  const [open, setOpen] = React.useState(false);

  let navigate = useNavigate();
  const reNavigateUser = () => {
    const auth = getAuth();
    const user = auth.currentUser;

    if (user != null) {
      let path = '/AboutMe/' + user.uid;
      navigate(path);
    }
  }

  return (
    <>
      <Collapse in={open}>
        <Alert
          severity="error"
          onClose={() => {
            setOpen(false);
          }}
        >
          Information is incorrect!
        </Alert>
      </Collapse>
      <div className="Login">
        <form className={classes.root}>
          <h className="title2">Welcome Back</h>

          <TextField
            label="Email"
            variant="outlined"
            type="email"
            required
            value={registerEmail}
            onChange={(e) => setRegisterEmail(e.target.value)}
            error={false}
          />
          <TextField
            label="Password"
            variant="outlined"
            type="password"
            required
            value={registerPassword}
            onChange={(e) => setRegisterPassword(e.target.value)}
            error={false}
          />

          <div>
            {/* <Button className="button2" onClick={signIn} variant="contained" style={{ backgroundColor: '#12565a' }}>
              Login
            </Button> */}
            <Button
              className="button2"
              variant="contained"
              style={{ backgroundColor: "#564BD1" }}
              onClick={() => {
                login(registerEmail, registerPassword);
                reNavigateUser();
              }}
            >
              Login
            </Button>
            <Divider style={{ color: "#12565a" }}>or</Divider>

            <br></br>
            <p className="logRout2">
              {" "}
              Don't have an account?
              <Link className="logRout_b2" to="/SignUp">
                {" "}
                Sign-Up
              </Link>
            </p>
          </div>
        </form>
      </div>
    </>
  );
};

export default LogIn;
