import React from "react";
import { Stack } from "@mui/system";
import { Button, IconButton } from "@mui/material";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import './Summary.css'
const Summary = () => {
  
    let navigate = useNavigate();
    const next = (e) => {
        let path = '/Review'

        e.preventDefault();
        const formData = new FormData ();
        formData.append('pdf', localStorage.getItem('pdf'));
        formData.append('documentName', localStorage.getItem("documentName"));
        formData.append('tags',localStorage.getItem("tags") )
        axios.post ('http://localhost:4000/api/templates',formData);



        navigate(path)
        
    }
    const prev = () => {
        let path = '/UploadQues'
        navigate(path)
    }
    const buttonSX ={
        backgroundColor: '#D9D9D9',
                    color: 'black',
                    padding: '1% 4%',
                    borderRadius: '5px',
                    "&:hover":{
                        backgroundColor: '#ffffff'
                    }
    }
    return (
    <>
    <Stack 
    spacing={3} 
    className='stack'>
            <h1>Summary</h1>
            <p>Make sure everything looks good before submitting!</p>
            <p>Anonymity: {localStorage.getItem("anon")} </p>
            <p>Tags: {localStorage.getItem("tags")}</p>
         
            {localStorage.getItem("resumeURL") && localStorage.getItem("pdf") 
            && <iframe className="frame" src={localStorage.getItem("resumeURL")} frameBorder="0" height="600" width="50%" />}




    </Stack>

        <div className='buttonsBottom'>
                <Button
                    onClick={prev}
                    sx={buttonSX}> Previous
                </Button>

                <Button
                    sx={buttonSX}
                    onClick={next}> Next</Button>
        </div>
        </>
    );
};
export default Summary;