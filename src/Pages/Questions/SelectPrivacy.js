import * as React from 'react';
import { useState } from 'react';
import Stack from '@mui/material/Stack';
import { Button } from '@mui/material';
import './SelectPrivacy.css';
import { useNavigate } from 'react-router-dom';
const SelectPrivacy = () => {
    
    const [colorPub,setColorPub] = useState('FF7171') 
    const [colorPriv,setColorPriv] = useState('D9D9D9')
    const [anon, setAnon] = useState(false) 

    let navigate = useNavigate();
    const next = () => {
        let path = '/AddTags'
        if(!anon) localStorage.setItem('anon', false)
        else localStorage.setItem('anon',true);
        navigate(path)
    }
    const prev = () => {
        let path = '/Review'
        navigate(path)
    }
    const handlePub = () => {
        setColorPub("FF7171")
        setColorPriv('D9D9D9')
        setAnon(false)
        localStorage.setItem("anon", false);
      };
    const handlePriv = () => {
        setColorPriv("FF7171")
        setColorPub("D9D9D9")
        setAnon(true)
        localStorage.setItem("anon", true)
    };
    const buttonSX ={
        backgroundColor: '#D9D9D9',
                    color: 'black',
                    padding: '1% 4%',
                    borderRadius: '5px',
                    "&:hover":{
                        backgroundColor: '#ffffff'
                    }
    }
    return (
        <>
        <Stack spacing={5} className='stack'>
            <h1>Privacy Settings</h1>
            <p> Privacy settings decide whether your resume will be displayed publicly under your profile.If private a direct link will need to be shared for people to view your resume </p>
            <div className='buttonsTop'>
                <Button sx={{
                   backgroundColor: `#${colorPub}`,
                    color: 'black',
                    padding: '1% 4%',
                    borderRadius: '5px',
                    "&:hover":{
                        backgroundColor: '#fc9e9e'
                    }

                }}
                onClick={handlePub}> Public</Button>
                <Button sx={{
                    backgroundColor: `#${colorPriv}`,
                    color: 'black',
                    padding: '1% 4%',
                    borderRadius: '5px',
                    "&:hover":{
                        backgroundColor: '#fc9e9e'
                    }
                  }}
                  onClick={handlePriv}
                  > Private</Button>
            </div>
        </Stack>
        <div className='buttonsBottom'>
                <Button sx={buttonSX} 
                onClick={prev}> Previous</Button>
                <Button sx={buttonSX}
                 onClick={next}> Next</Button>
        </div>
        </>
        
    );
};
export default SelectPrivacy;