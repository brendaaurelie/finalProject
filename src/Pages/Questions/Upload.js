import React from "react";
import { useState,useEffect } from "react";
import { Stack } from "@mui/system";
import { Button, IconButton } from "@mui/material";
import { useNavigate } from "react-router-dom";
import AddIcon from '@mui/icons-material/Add';
import './Summary.css'
import axios from "axios";
import { getAuth } from "firebase/auth";


const UploadQues = () => {
    const [pdf, setPdf] = useState('')
    let navigate = useNavigate();
    const [resumeURL, setResumeUrl] = useState(null);
    
    const next = () => {
        let path = '/Review'
        navigate(path)
    }
    const prev = () => {
        let path = '/AddTags'
        navigate(path)
    }
    
    const buttonSX ={
        backgroundColor: '#D9D9D9',
                    color: 'black',
                    padding: '1% 4%',
                    borderRadius: '5px',
                    "&:hover":{
                        backgroundColor: '#ffffff'
                    }
    }
      
    const handleUpload = (e) => {
        let filename = e.target.files[0].name;
        setPdf(e.target.files[0])
        setResumeUrl(URL.createObjectURL(e.target.files[0]));
        localStorage.setItem("documentName",filename)
        localStorage.setItem("pdf", e.target.files[0])
        localStorage.setItem("resumeURL", URL.createObjectURL(e.target.files[0]))
    }

    const handleFormSubmittion = (e) =>
    {
        e.preventDefault();
        const auth = getAuth();
        const user = auth.currentUser;
        const currentUserId = user.uid;
    
        const formData = new FormData ();
        formData.append('firebaseId', currentUserId);
        formData.append('anonymity',localStorage.getItem("anon"))
        formData.append('pdf', pdf);
        formData.append('documentName', localStorage.getItem("documentName"));
        formData.append('tags',localStorage.getItem("tags") )
        axios.post ('http://localhost:4000/api/resumes',formData);
        console.log("form uploaded")
    }
    



    

    return (
    <>
      <form onSubmit={handleFormSubmittion} enctype="multipart/form-data">
    <Stack 
    spacing={5} 
    className='stack'>
            <h1>Upload Your Resume</h1>
            <p>Upload your resume. Remove your name if you want to protect your anonymity.</p>
          
          
            
            <Button 
            variant="contained" 
            component="label" 
            startIcon={<AddIcon />}
         
            sx={{
                backgroundColor: '#FF7171',
                color: 'black',
                padding: '1% 4%',
                borderRadius: '5px',
                "&:hover":{
                    backgroundColor: '#fc9e9e'
                }

            }}
           >
                <input hidden type="file" name="pdf" onChange={handleUpload} />

            </Button>
            <Button type="submit"> Submit</Button>
           

    </Stack>
    </form>
    {pdf && resumeURL &&  
    <iframe className="frame" src={resumeURL} frameBorder="0" height="600" width="50%" />
    }

        <div className='buttonsBottom'>
                <Button
                    onClick={prev}
                    sx={buttonSX}> Previous
                </Button>

                <Button
                    sx={buttonSX}
                    onClick={next}> Done</Button>
        </div>
        </>
    );
};
export default UploadQues;