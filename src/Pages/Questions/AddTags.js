import * as React from 'react';

import Stack from '@mui/material/Stack';
import { Theme, useTheme } from '@mui/material/styles';
import { useState, useEffect } from 'react';
import Box from '@mui/material/Box';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import Chip from '@mui/material/Chip';
import { Button } from '@mui/material';
import {  useNavigate } from 'react-router-dom';
import './AddTags.css';

const tags = [
    'Gradschool',
    'Undergrads',
    'Full-Time',
    'Part-Time',
    'Internship',
    'Software Engineer',
    'Product Management',
    'PHD',
    'Mechanical Engineering',
    'Math',
  ];
  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };
  
  function getStyles(tags, tagName, theme) {
    return {
      fontWeight:
      tagName.indexOf(tags) === -1
          ? theme.typography.fontWeightRegular
          : theme.typography.fontWeightMedium,
    color: "white",
    background: "black",
    };
  }
const AddTags = () => {
    const theme = useTheme();
    const [tagName, setTagName] = useState([]);

    let navigate = useNavigate();
const next = () => {
    let path = '/UploadQues'
    navigate(path)
}
const prev = () => {
    let path = '/SelectPrivacy'
    navigate(path)
}
const buttonSX ={
    backgroundColor: '#D9D9D9',
                color: 'black',
                padding: '1% 4%',
                borderRadius: '5px',
                "&:hover":{
                    backgroundColor: '#ffffff'
                }
}
useEffect(() => {
    localStorage.setItem("tags", tagName)
  },[tagName]);

  
    const handleChange = (event) => {
      const {
        target: { value },
      } = event;
      setTagName(
        // On autofill we get a stringified value.
        typeof value === 'string' ? value.split(',') : value,
      );
     
    };
    return (
        <div>
        <Stack spacing={5} className="stack">
        <h1>Add relevant tags to your resume</h1>
        <p>Think about tags like answering the question "I am a ..." </p>
       
        <div className='selectTags' >
        <FormControl className='form' sx={{ m: 1, width: 300 }}>
          <InputLabel id="demo-multiple-chip-label">Tags</InputLabel>
          <Select
            labelId="demo-multiple-chip-label"
            id="demo-multiple-chip"
            multiple
            value={tagName}
            onChange={handleChange}
            input={<OutlinedInput id="select-multiple-chip" label="Chip" />}
            renderValue={(selected) => (
              <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                {selected.map((value) => (
                  <Chip 
                  key={value} 
                  label={value}
                  style={{backgroundColor:'#FF7171'}}
                 />
                ))}
              </Box>
            )}
            MenuProps={MenuProps}
          >
            {tags.map((tag) => (
              <MenuItem
                key={tag}
                value={tag}
                style={getStyles(tag, tagName, theme)}
              >
                {tag}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        </div>
        </Stack>
        <div className='buttonsBottom'>
                <Button 
                onClick={prev}
                sx={buttonSX}> Previous
                </Button>

                <Button 
                sx={buttonSX}
                onClick={next}> Next</Button>
        </div>
       
      </div>
    );
};
export default AddTags;