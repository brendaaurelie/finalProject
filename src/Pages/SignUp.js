import React from "react";
import Button from "@mui/material/Button";
import { useState, useEffect } from "react";
import { TextField } from "@mui/material";
import { makeStyles } from "@material-ui/core";
import "./SignUp.css";

import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";

import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import { signUp } from "../auth.js";


//Pop up the sign up component when clicked, close otherwise
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: theme.spacing(10),
    backgroundColor: "white",
    "& .MuiTextField-root": {
      margin: theme.spacing(2),
      width: "300px",
    },
    "& .MuiButtonBase-root": {
      margin: theme.spacing(2),
      width: "300px",
    },
  },
}));

const SignUp = () => {
  const classes = useStyles();
  const [user, setUser] = useState({});
  const [registerEmail, setRegisterEmail] = useState("");
  const [registerPassword, setRegisterPassword] = useState("");
  const [success, setSuccess] = useState(false);
  const [redirect, setredirect] = useState(null);

  let navigate = useNavigate();
  const reNavigateUser = () => {
    const auth = getAuth();
    const user = auth.currentUser;

    if (user != null) {
      let path = '/AboutMe/' + user.uid;
      navigate(path);
    }
  }

  return (
    <div className="SignUp">
      <form className={classes.root}>
        <h className="title">LlamaBear</h>
        <TextField
          label="Email"
          variant="outlined"
          type="email"
          required
          value={registerEmail}
          onChange={(e) => setRegisterEmail(e.target.value)}
        />
        <TextField
          label="Password"
          variant="outlined"
          type="password"
          required
          value={registerPassword}
          onChange={(e) => setRegisterPassword(e.target.value)}
        />
        <div>
          {/* <Button onClick={register} className="button1" variant="contained" style={{backgroundColor:'#12565a'}} >
            Sign Up
          </Button> */}
          <Button
            className="button1"
            variant="contained"
            style={{ backgroundColor: "#564BD1" }}
            onClick={() => {
              signUp(registerEmail, registerPassword);
              reNavigateUser();
            }}
          >
            Sign Up
          </Button>
          <p className="logRout">
            {" "}
            Already have an account?
            <Link className="logRout_b" to="/Login">
              {" "}
              Log-In
            </Link>
          </p>
        </div>
      </form>
    </div>
  );
};

export default SignUp;
