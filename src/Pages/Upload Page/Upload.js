import "./Upload.css"
import React, { useEffect } from "react";
import { FaUpload } from "react-icons/fa";
import { useState } from "react";
import { Link } from "react-router-dom"
import { Chip } from "@mui/material";
import { getAuth } from "firebase/auth";
import Box from "@mui/material/Box";
import Axios from "axios";

const Upload = () => {
    const [uploadList, setUploadList] = useState([]);
    // lilians id: aiPYBuAK5oTf3OJ2ycBP0eTjlAg2
    const auth = getAuth();
    const user = auth.currentUser;
    const currentUserId = user.uid;

    let fullPdfList = []

    useEffect(() => {
        populatePdfList();
    }, [])
    
    let populatePdfList = () => {
        Axios.get(`https://finalproject-backend.vercel.app/api/resumes/?where={"firebaseId":"${currentUserId}"}`).then((response) => {
            console.log(response);
            response.data.data.forEach(element => {
                let src = `https://finalproject-backend.vercel.app/api/resumes/pdf/${element._id}`;

                const pdf = ({
                    resume_name: element.documentName,
                    pdf_src: src,
                    tags: element.tags.map(tag => {
                        return tag.toLowerCase();
                    }),
                    path: `/Resume/${element._id}`
                })
                // console.log(pdf)
                fullPdfList = [...fullPdfList, pdf];
                setUploadList(fullPdfList)
            });
        })
        console.log("here ;-;")
    }

    return (
        <div id="upload-container">
            <h1 className="header">My Resumes</h1>
            {/* <button onClick={hardCodeUploadList}>Hard Code Populate List</button> */}
            <div className="side-by-side-container">
                
                <div className="upload-button">
                    <Link to="/SelectPrivacy">
                        <div className="upload">
                            <h4>Upload New Resume</h4>
                            <FaUpload id='upload-icon' />
                        </div>
                    </Link>
                </div>
                <div className="upload-grid">
                    {
                        uploadList.map((item, index) => (
                            <Link to={item.path} key={index}>
                                <div className="pdf-card">
                                    <embed src={item.pdf_src} type="application/pdf" className="pdf" />
                                    <h3 className="resume-name">{item.resume_name}</h3>
                                    <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                                        {item.tags.map((value) => (
                                            <Chip
                                                key={value}
                                                label={value}
                                                style={{ backgroundColor: '#FF7171', marginBottom: '1%' }}
                                            />
                                        ))}
                                    </Box>
                                </div>
                            </Link>
                        ))
                    }
                </div>
            </div>
        </div>
    )

};

export default Upload;
