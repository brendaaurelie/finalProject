import { Routes, Route, Outlet } from "react-router-dom";
import './App.css';
import { Navigate, Switch,BrowserRouter } from 'react-router-dom';
import SignUp from "./Pages/SignUp";
import LogIn from "./Pages/LogIn";
import Landing from "./Pages/LandingPage/Landing";
import AddTags from "./Pages/Questions/AddTags";
import SelectPrivacy from "./Pages/Questions/SelectPrivacy";
import UploadQues from "./Pages/Questions/Upload";
import Summary from "./Pages/Questions/Summary";
import AboutMe from "./Pages/AboutMePage/AboutMe";
import SideNav from "./Components/SideNav/SideNav";
import Review from "./Pages/Review Page/Review"
import Upload from "./Pages/Upload Page/Upload"
import Templates from "./Pages/TemplatePage/Templates";
import ResumeView from "./Pages/ResumeView/ResumeView";

const SideNavNest = () => (
  <>
  <div>
    <SideNav>
    <Outlet />
    </SideNav>
  </div>
  </>
);

function App() {
  return (
    <Routes>
      <Route path="/" element={<Landing />} />,
      <Route path="/Landing" element={<Landing />} />,
      <Route path="/Login" element={<LogIn />} />,
      <Route path="/SignUp" element={<SignUp />} />,
      <Route path="/AddTags" element={<AddTags />} />,
      <Route path="/SelectPrivacy" element={<SelectPrivacy />} />,
      <Route path="/UploadQues" element={<UploadQues />} />,
      <Route path="/Summary" element={<Summary />} />,
      <Route path="/Resume/:id" element={<ResumeView />} />,
      <Route element={<SideNavNest />}>
        <Route path="/AboutMe/:id" element={<AboutMe />} />
        <Route path="/Review" element={<Review/>}/>
        <Route path="/Upload" element={<Upload/>}/>
        <Route path="/Templates" element={<Templates />} />
      </Route>
    </Routes>

  );
}

export default App;
